# Envision - Computer Vision Assignment - July 2021

## Problem Statement

One of the biggest challenges that our visually impaired users face when it comes to using the Envision app or the Glasses is being able to take a proper image of the document they're trying to scan. If you've used apps like CamScanner or Office Lens you're aware of how scanning a document with a phone works. And right now, it's only optimised for sighted individuals who can see the document and place the phone right above it to take a perfect picture.

Your assignment will be to build a more acccessible version of this document scanning system for visually impaired people. We've solved this ourselves on the Envision Glasses and are really curious about your implementation.

Essentially, given an image of an A4 document, we'd like to build a model which can return which corners are visible. Using this model we'll guide users on the best way to capture an image before scanning it with Envision. You'll be working with engineers and designers on this last part but that's out of the scope of this assignment.

## Expected Outcome

We'd like you to build a model/algorithm which accepts an image of an A4 document and returns corners of the document that are visible along with the labels/bounding boxes. The assignment can be in Python and should use Tensorflow/Keras or Pytorch. For any image processing work please use OpenCV. 

When doing inference with the model, ideally we would like to get predictions that look similar to the sample below. Any relevant metrics that can evaluate the performance of your model output are welcome too.

### Corner/Keypoint Detection

Any corner visible in the image should be marked as a corner. 

![](0_corner_detection_sample.jpg)

## What's Given?

For this assignment it's mandatory to use the dataset '*A New Image Dataset for Document Corner Localization*' as the training data for your model. This dataset (version 3) can be found below. Make sure you download the latest version (v3) of the dataset: https://data.mendeley.com/datasets/x3nm4cxr83/3

Adding more data from different datasets is not recommended (as this will be time consuming). Augmenting this current dataset is allowed if you think it is useful.

The original paper that came with the dataset is also provided in this repo. A dataset quick viewer script is provided to be able to quickly have a look at the images and the corresponding labels (`dataset_viewer.py`). The dataset itself can have some labelling noise/mistakes, handle if you think it is useful.

## Sharing your work

You can share your work in the following ways after forking this repo: 

- Google Colab links(makes training easier as well)
- Standalone, executable scripts.
- Jupyter Notebook / other hosted services.

Please also provide a readme with a few lines about a quickstart on how we can test/evaluate your model/algorithm.

## Bonus

Detecting the corners is the basic assignment in itself but there are a couple of bonus tasks which would make your work truly standout.

### Bonus A (Corner classification)

Once you have a feasible approach for corner detection, ideally we would also like to classify these corners. The labels for this are:
- TL (top-left)
- BL (bottom-left)
- BR (bottom-right)
- TR (top-right)

You can use either a heuristics or deep learning based approach for this.

![](1_corner_classification_sample.jpg)

### Bonus B (Deployable model)

Build a deployable version of the model you're building. If you're building the model using Tensorflow, you can present a TFlite model. If you're using Pytorch then you can give the new Pytroch Mobile Interpreter a shot. You can take a look at the various quantization options available for TFlite and Pytorch Mobile.

## Deadline.

1 week. If you think you need more time, please let me know about it. 

