import pandas as pd
import numpy as np
import cv2
import os

dataset_path = './2020_document_dataset/'
dataset_size = 1111

CORNER_LABELS = ['TL','BL', 'BR', 'TR']

def show_label_on_image(img, label):
    """ Takes in an image and corresponding label and returns a labeled image """

    label_count = 0
    df = label
    corner_locations = df.columns

    font                   = cv2.FONT_HERSHEY_SIMPLEX
    bottomLeftCornerOfText = (10,500)
    fontScale              = 3
    fontColor              = (255,255,255)
    lineType               = 2

    for i in range(0, len(corner_locations) - 2, 2):

        corner_x = int(corner_locations[i].split('.')[0])
        corner_y = int(corner_locations[i+1].split('.')[0])
        img = cv2.circle(img, (corner_x, corner_y), radius=30, color=(0, 0, 255), thickness=-1)
        result = cv2.putText(img,CORNER_LABELS[label_count], 
                            (corner_x+30, corner_y+30), 
                            font, 
                            fontScale,
                            fontColor,
                            lineType)
        label_count += 1

    return result

# 2020 document dataset start with 1.jpg
for i in range(1, 1111):
    # sometimes, a few numbers are skipped in the labels
    if os.path.isfile(dataset_path + '(' + str(i) + ')' + '.jpg'):
        print('image: ' + str(i))
        img =  cv2.imread(dataset_path + '(' + str(i) + ')' + '.jpg')
        label = pd.read_csv(dataset_path + '(' + str(i) + ')' + '.csv')
        annotated_img = show_label_on_image(img.copy(), label)
        cv2.imshow('sample', annotated_img)
        cv2.waitKey()


